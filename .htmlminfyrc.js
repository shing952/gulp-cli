module.exports = {
  removeComments: true, //清除HTML注释
  collapseWhitespace: true, //压缩HTML
  collapseBooleanAttributes: false, //省略布尔属性的值 <input checked="true"/> ==> <input checked />
  removeEmptyAttributes: true, //删除所有空格作属性值 <input id="" /> ==> <input />
  removeScriptTypeAttributes: false, //删除<script>的type="text/javascript"
  removeStyleLinkTypeAttributes: false, //删除<style>和<link>的type="text/css"
  minifyJS: true, //压缩页面JS
  minifyCSS: true //压缩页面CSS
}
