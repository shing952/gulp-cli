module.exports = {
  "root": false,
  "env": {
    "browser": true
  },
  "extends": "eslint:recommended",
  "rules": {
    "indent": [
      "error",
      2
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "warn",
      "single"
    ],
    "semi": [
      "error",
      "always"
    ],
    "no-undef": "off",
    "no-unused-vars": "off",
    "no-console": "off"
  }
};
